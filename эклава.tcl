﻿encoding system utf-8

# Try to load record_definition as early as possible to get access to all
# source locations
namespace eval ::ehklava {
    variable ScriptDirectory 
    set ScriptDirectory [file dirname [info script]]
}


proc ::ehklava::CheckPatchlevel {} {
    regsub -all {[a-zA-Z.]} [info patchlevel] {.} dd
    set p [split $dd {.}]
    set ma [lindex $p 0]
    set mi [lindex $p 1]
    set su [lindex $p 2]
    if { $ma < 8 || $mi < 6 || $su < 2 } {
        error "Clcon requires Tcl/Tk >= 8.6.2"
    }
}

 ::ehklava::CheckPatchlevel


## TkconSourceHere - buddens command to load file from the same dir where
# clcon itself is located. Be sure to load into main interpreter when you need it:
#
proc TkconSourceHere { filename } {
    variable ::ehklava::ScriptDirectory
    source [file join $::ehklava::ScriptDirectory $filename]
}

package require Tk 8.4

option add *tearOff 0

# Unset temporary global vars
catch {unset pkg file name version}

namespace eval ::mprs {
    proc ::mprs::AssertEq {x y {note {}}} {
        if {! ($x eq $y)} {
            error "Assertion failure:$note: $x eq $y"
        }
}

}

# Initialize the ::ehklava namespace
#
namespace eval ::ehklava {
    # The OPT variable is an array containing most of the optional
    # info to configure.  COLOR has the color data.
    variable OPT
    variable COLOR

    # PRIV is used for internal data that only tkcon should fiddle with.
    variable PRIV
    set PRIV(WWW) [info exists embed_args]
    set PRIV(AQUA) [expr {$::tcl_version >= 8.4 && [tk windowingsystem] == "aqua"}]
    set PRIV(CTRL) [expr {$PRIV(AQUA) ? "Command-" : "Control-"}]
    set PRIV(ACC) [expr {$PRIV(AQUA) ? "Command-" : "Ctrl+"}]

    variable EXPECT 0
   

}

TkconSourceHere gui_util.tcl
TkconSourceHere clcon_key.tcl

proc ::ehklava::Init {args} {
    variable OPT
    variable COLOR
    variable PRIV
    global tcl_platform env 

    set argc [llength $args]

    ##
    ## When setting up all the default values, we always check for
    ## prior existence.  This allows users who embed tkcon to modify
    ## the initial state before tkcon initializes itself.
    ##

    # bg == {} will get bg color from the main toplevel (in InitUI)
    foreach {key default} {
	bg		{}
	cursor		\#000000
	fg		\#FFC0D0
    } {
	if {![info exists COLOR($key)]} { set COLOR($key) $default }
    }

    if {![info exists OPT(шрифт)]} {
        if {$tcl_platform(platform) == "windows"} {
            set OPT(шрифт) {{Courier New} 12}
        } else {
            set OPT(шрифт) {{Courier New} 12}
	}
    }

    ## Do platform specific configuration here, other than defaults
    ### Use tkcon.cfg filename for resource filename on non-unix systems
    ### Determine what directory the resource file should be in
    switch $tcl_platform(platform) {
	windows		{
	    set envHome		HOME
	    set rcfile		эклава-настройки.tcl
            set desktopfile     эклава-настройки-окна.cfg
	}
	unix		{
	    set envHome		HOME
	    set rcfile		.config/эклава/настройки.tcl
            set desktopfile     .config/эклава/настройки-окна.cfg
	}
    }
    if {[info exists env($envHome)]} {
	set home $env($envHome)
	if {[file pathtype $home] == "volumerelative"} {
	    # Convert 'C:' to 'C:/' if necessary, innocuous otherwise
	    append home /
	}
	if {![info exists PRIV(rcfile)]} {
	    set PRIV(rcfile)	[file join $home $rcfile]
	}
    }

    ## Загрузка настроек
    if {!$PRIV(WWW) && [file exists $PRIV(rcfile)]} {
	set code [catch {uplevel \#0 [list source $PRIV(rcfile)]} err]
    }

    ## Handle rest of command line arguments after sourcing resource file
    ## but before initializing UI
    for {set i 0} {$i < $argc} {incr i} {
	set arg [lindex $args $i]
	if {[string match {-*} $arg]} {
	    set val [lindex $args [incr i]]
	    ## Handle arg based options
	    switch -glob -- $arg {
		-color-*	{ set COLOR([string range $arg 7 end]) $val }
		-font		{ set OPT(шрифт) $val }
		default	{ 
		    puts "Неизвестный аргумент $arg"
		    exit 1 
		}
	    }
	} else {
	    puts "Неизвестный аргумент $arg"
	    exit 1 
	}
    }

    ::ehklava::InitUI 

}

proc ::ehklava::InitUI {} {

    set root . 
    wm withdraw $root

    catch {font create tkconfixed -family Courier -size -20}
    catch {font create tkconfixedbold -family Courier -size -20 -weight bold}

    ::ehklava::Показать_экранную_клавиатуру $root
}

TkconSourceHere eval.tkcon.tcl
TkconSourceHere экранная-клавиатура.tcl

eval ::ehklava::Init $argv
