package require Tk

namespace eval ::gui_util {

    # Sets some sensible fonts for text widget
    proc ConfigureTextFonts {text} {
        variable ::ehklava::COLOR
        $text configure \
            -foreground $COLOR(fg) \
            -background $COLOR(bg) \
            -font $::ehklava::OPT(шрифт) -borderwidth 1 -highlightthickness 0 \
            -undo 1
    }

    # Returns list of lists three elements: bindtag event script
    proc ListAllWindowBindings { w } {
        set result {}
        foreach bindtag [bindtags $w] {
            foreach binding [bind $bindtag] {
                lappend result \
                    [list $bindtag $binding \
                         [bind $bindtag $binding]]
            }
        }
        return $result
    }

    # Adds puts (entry) to each binding
    # Example:  ::gui_util::InstrumentAllBindingsWithPutd [::edt::c_btext]
    # ::gui_util::InstrumentAllBindingsWithPutd [::edt::c_text] 454570000
    # As binding fires, you have string like InstrumentAllBindingsWithPutd/454560029 in your stdout.
    # Then call .. ::gui_util::ListAllWindowBindings [::edt::c_btext] and find number there with console Find box. 
    proc InstrumentAllBindingsWithPutd { w {InitialNumber 454560000}} {
        foreach entry [ListAllWindowBindings $w] {
            foreach { bindtag event script } $entry {}
            set script [string cat "puts InstrumentAllBindingsWithPutd/[incr InitialNumber];" $script]
            bind $bindtag $event $script
        }
        return $InitialNumber
    }

    proc FocusWindowByName {window {widget {}}} {
        if {$window eq {}} {
            bell
            return
        } 
        set code [catch {
            set state_now [wm state $window]
            if {[lsearch {iconic withdrawn} $state_now] >= 0} {
                wm deiconify $window
            }
            raise $window
            if {$widget ne {}} {
                focus $widget
            } else {
                focus $window
            }
        }]
        if {$code} {
            bell
        }
        return 
    } 
}

