## This file is a part of clcon. Eval and friends. 

namespace eval ::ehklava {

    ## ::ehklava::Eval - evaluates commands input into console window
    ## This is the first stage of the evaluating commands in the console.
    ## They need to be broken up into consituent commands (by ::ehklava::CmdSep) in
    ## case a multiple commands were pasted in, then each is eval'ed (by
    ## ::ehklava::EvalCmd) in turn.  Any uncompleted command will not be eval'ed.
    # ARGS:	w	- console text widget
    # Calls:	::ehklava::CmdGet, ::ehklava::CmdSep, ::ehklava::EvalCmd
    ## 
    proc Eval {w} {
        variable PRIV
        set gotcmd [CmdGet $w]
        
        set complete [CmdSep $gotcmd cmds last]
        $w mark set insert end-1c
        $w insert end \n
        if {[llength $cmds]} {
            foreach c $cmds {EvalCmd $w $c}
            $w insert insert $last {}
        } elseif {$complete} {
            EvalCmd $w $last
        }
        if {[winfo exists $w]} {
            $w see insert
        }
    }

    proc RePrompt {{pre {}} {post {}} {prompt {}}} {
        # same as prompt, but does nothing for those actions where we
        # only wanted to refresh the prompt on attach change when the
        # statusbar is showing (which carries that info instead)
        variable OPT
        if {!$OPT(showstatusbar)} {
            Prompt $pre $post $prompt
        }
    }

}
