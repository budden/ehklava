# -*- coding: utf-8; -*- 
# по мотивам http://wiki.tcl.tk/560
# (С) Денис Будяк, Роман Клочков, 2017-2023
# Здесь гвоздями прибито имя .ЭкраннаяКлавиатура, будьте осторожны!
namespace eval ::ehklava {

#-title string: If not "", text of a title label displayed above the keys. Default: "".
#-receiver widgetpath: Name of a text widget to receive the keystrokes at its insert cursor.

# В макете клавиатуры используются обозначения:
# откр-фигурная-скобка = {
# закр-фигурная-скобка = }
# вертикальная-черта = |

 set имяФайлаИндивидуальныхНастроек "~/.config/эклава/настройки.tcl"
 if {[file exists ${имяФайлаИндивидуальныхНастроек}]} {
   # ничего не делаем, файл настроек найден и загружен
 } else {
   puts "Файл индивидуальных настроек ${имяФайлаИндивидуальныхНастроек} не существует, продолжаю с настройками по умолчанию"

 variable МакетКлавиатурыОбщий {
|ё`|1г|2Г|3§|4$|5°|6^|7&|8₽|9„|0“|-—|=≡|
| |й✓|ц☼|у×|к†|е |н¬|гГ|ш |щ∃|з |[⊆|]закр-фигурная-скобка|\⇒|
|  |фΩ|ы |в⊢|аₒ|п∈|р÷|о |л∨|д∀|ж |" ||
|   |я |чₓ|с♥|м⊨|и∧|тØ|ьЪ|б«|ю»|/ |
}

 variable МакетКлавиатурыГреческий {
|``|1|2Г|3§|4$|5°|6закр-фигурная-скобка|7откр-фигурная-скобка|8₽|9|0-|-—|=вертикальная-черта|
| |й |ц |у×|кκ|tε|y |uγ|шθ|щ |зζ|[χξ|]»|  |
|  |фφ|ыψ|в |аα|пπ|р÷|о |лλ|дδ|ж |эη|\∫|
|   |яω|чϰ|сσ|мμ|иι|тτ|ь |,β|. |/ |
}

 variable МакетКлавиатурыГреческийЗаглавный {
|``|1г|2|3§|4$|5°|6закр-фигурная-скобка|7откр-фигурная-скобка|8₽|9|0-|-—|=вертикальная-черта|
| |й |ц |у×|кκ|tε|y |uγ|шΘ|щ |зζ|[χΞ|]»|  |
|  |фΦ|ыΨ|в |аα|пπ|р÷|о |лΛ|дΔ|ж |э|\∫|
|   |яΩ|чϰ|сΣ|мμ|иι|тτ|ь |,β|. |/ |
}


    variable задержка,мс 300
 }


proc превратиРусскоеИзображениеВАнглийское {inputChar} {
    set letters "ёйцукенгшщзхъфывапролджэячсмитьбю"
    set replacements {`qwertyuiop[]asdfghjkl;"zxcvbnm,.}
    
    set index [string first $inputChar $letters]
    if {$index != -1} {
        set outputChar [string index $replacements $index]
        return $outputChar
    } else {
        return $inputChar
    }
}

 proc keyboard {w args replaceP макетКлавиатуры} {
   variable ${макетКлавиатуры}
   if {$replaceP} {
    destroy $w
   }

   frame $w
   array set opts { -title "" -receiver "" }
   array set opts $args ;# no errors checked 
   set klist {}; set n 0
   if {$opts(-title)!=""} {
      grid [label $w.title -text $opts(-title) ] 
      }
   set j 0
   array set seen {}
   set r 1
   frame $w.row$r
   grid $w.row$r -sticky w
   foreach i [split [set ${макетКлавиатуры}] "|"] {
      set i [string map {вертикальная-черта "|"} $i]
      set i [string map {откр-фигурная-скобка "\{"} $i]
      set i [string map {закр-фигурная-скобка "\}"} $i]
      # puts "После замены обозначений, клавиша выглядит так: $i"
      set c [string index [string trim $i] 1]      
      set русскаяКнопка [string index [string trim $i] 0]
      set key [превратиРусскоеИзображениеВАнглийское ${русскаяКнопка}]
      # puts "русская кнопка ${русскаяКнопка} превращается в английскую ${key}"
      if {$key == "`"} {set key quoteleft
      } elseif {$key == "-"} {set key minus
      } elseif {$key == "\""} {set key quoteright
      } elseif {$key == ","} {set key comma
      } elseif {$key == "."} {set key period
      } elseif {$key == "/"} {set key slash
      } elseif {$key == ";"} {set key semicolon
      } elseif {$key == {[}} {set key bracketleft
      } elseif {$key == {]}} {set key bracketright
      } elseif {$key == {=}} {set key equal
      }

      set empty [expr {$key == ""}]
      set newline [expr {"\n" eq [string index $i [expr [string length $i]-1]]}]
      if {$newline} {
        set i [string range $i 0 [expr [string length $i]-2]]
      }
      if {!$empty && [info exists seen($key)]} {
        error "Кнопка $key уже есть"
      }
      set seen($key) 1
      # puts "кнопке $key будет назначен символ [list $c]"
      set cmd [list ::ehklava::спрячьсяВыведиИЗавершись .ЭкраннаяКлавиатура $c]
      if {$newline} {
        if {!($klist == "")} {
          eval grid $klist -sticky news
          set n 0; set klist {}
          set r [expr $r + 1]
          frame $w.row$r
          grid $w.row$r -sticky w
        }
      } else {
        if {($c == "-") && ($key == "0")} {
           set cmd1 [list ::ehklava::keyboard $w {} 1 ::ehklava::МакетКлавиатурыОбщий]
           button $w.row$r.k$j -text $i -command $cmd1  -padx 0 -pady 0 -font $::ehklava::OPT(шрифт) -background Orange
           set Key1 [string cat "<Key-" $key ">"]
           if {!$empty} {
             ::clcon_key::b bind .ЭкраннаяКлавиатура $Key1 $cmd1
           }
        } elseif {($c == "г") && ($key == "1")} {
           set cmd1 [list ::ehklava::keyboard $w {} 1 ::ehklava::МакетКлавиатурыГреческий]
           button $w.row$r.k$j -text $i -command $cmd1  -padx 0 -pady 0 -font $::ehklava::OPT(шрифт) -background Orange
           set Key1 [string cat "<Key-" $key ">"]
           if {!$empty} {
             ::clcon_key::b bind .ЭкраннаяКлавиатура $Key1 $cmd1
           }
        } elseif {($c == "Г") && ($key == "2")} {
           set cmd1 [list ::ehklava::keyboard $w {} 1 ::ehklava::МакетКлавиатурыГреческийЗаглавный]
           button $w.row$r.k$j -text $i -command $cmd1  -padx 0 -pady 0 -font $::ehklava::OPT(шрифт) -background Orange
           set Key1 [string cat "<Key-" $key ">"]
           if {!$empty} {
             ::clcon_key::b bind .ЭкраннаяКлавиатура $Key1 $cmd1
           }
        } elseif {[string trim $i] == ""} {
           button $w.row$r.k$j -text $i -padx 0 -pady 0 -font $::ehklava::OPT(шрифт)
        } else {
           button $w.row$r.k$j -text $i -command $cmd  -padx 0 -pady 0 -font $::ehklava::OPT(шрифт)
           set Key [string cat "<Key-" $key ">"]
           if {!$empty} {
             ::clcon_key::b bind .ЭкраннаяКлавиатура $Key $cmd
           }
        }
        lappend klist $w.row$r.k$j
      }
      set j [expr $j + 1]
    }
    if [llength $klist] {eval grid $klist -sticky news}
    pack $w
    set w ;# return widget pathname, as the others do
 }

# это работает, но мы сделали более хороший вариант через xdtotool
 proc запишиВБуферОбменаИЗавершись1 {w c} {
    set command "| echo -n $c | xclip -selection clipboard"
    set pipe [open $command r]
    close $pipe
    exit
 }

# это вариант из копирования tkcon, но не работает (т.к. мы не владеем выдлеением)
 proc запишиВБуферОбмена2 {w c} {
  if {[string match $w [selection own -displayof $w]]} {
      clipboard clear -displayof $w
      catch {
          set txt [selection get -displayof $w]
          clipboard append -displayof $w $txt
      }
  } else {
    # puts "Выделение не принадлежит текущему окну"
  }
  exit
  }

# это работает, но слишком медленно и работоспособность зависит от того, сколько подождать. 
 proc запишиВБуферОбменаИЗавершись3 {w c} {
  clipboard clear -displayof $w
  clipboard append -displayof $w $c
  destroy $w
  after 500 exit
 }

 # это вариант, который используется
 proc спрячьсяВыведиИЗавершись {w c} {
  # puts "Заказано вывести знак $c"
  variable задержка,мс
  set code_point [scan $c %c]
  if {$code_point != ""} {
    set escape [format "U%04x" $code_point]
    # puts $escape ;# Выводит '\u044e'  
  } else { 
    set escape U03F 
    # вопросительный знак "?" означает ошибку на этом этапе
  }
  wm withdraw $w
  after ${задержка,мс} [list ::ehklava::спрячьсяВыведиИЗавершисьПродолжение1 $w $escape]
 }

 proc спрячьсяВыведиИЗавершисьПродолжение1 {w escape} {
  # puts "Продолжение 1 $w $escape"
  set arg "xdotool key --clearmodifiers \"\$(printf '$escape')\""
  # puts "Команда xdotool выглядит так: $arg"
  exec bash -c "$arg"
  exit
 }
  

 proc clist2list {clist} {
    #-- clist: compact integer list w.ranges, e.g. {1-5 7 9-11}
    set res {} 
    foreach i $clist {
        if [regexp {([^-]+)-([^-]+)} $i -> from to] {
            for {set j [expr $from]} {$j<=[expr $to]} {incr j} {
                lappend res $j
            }
        } else {lappend res [expr $i]}
    }
    set res
 }

 proc приУдаленииОкнаЭклавы {} {
  exit
 }

 # Правьмя. Нужно передавать команду для вставки нажатой кнопки в приёмник. 
 # Например, с помощью этой клавиатуры можно вставить текст в историю консоли, что, очевидно, 
 # неверно. Также можно вставить букву в замороженный текст. 
 # А также нужно, чтобы клавиатура всплывала  не на соседнем мониторе, а вблизи курсора клавиатуры ПРАВЬМЯ
 proc Показать_экранную_клавиатуру {Приёмник} {
     set w .ЭкраннаяКлавиатура
     toplevel $w
     wm protocol $w WM_DELETE_WINDOW ::ehklava::приУдаленииОкнаЭклавы     
     wm title $w "Эклава"

    # If we can locate the XDG icon file then make use of it.
    if {[package vsatisfies [package provide Tk] 8.6]} {
        if {[tk windowingsystem] eq "x11"} {
            # variable ScriptDirectory 
            ## иконка по умолчанию для короткоживущих окон
            set icon_filename [file join $::ehklava::ScriptDirectory "icon.png"]
            image create photo tkcon_icon -file $icon_filename
            wm iconphoto $w tkcon_icon
        }
    }


     bind $w <Escape> exit
     ::clcon_key::b bind $w <Control-Key-W> exit
     ::ehklava::keyboard $w.kbd {} 0 ::ehklava::МакетКлавиатурыОбщий
     ::gui_util::FocusWindowByName $w
     grab $w
    } 
}
