SCRIPT_PATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
mkdir -p ~/.config/эклава

# можно включить вывод в лог, если есть какие-то проблемы
Vyvod_v_log=0

if [[ $Vyvod_v_log == 1 ]]; then
  wish $SCRIPT_PATH/эклава.tcl $@ > ~/.config/эклава/эклава.log 2>&1
else
  wish $SCRIPT_PATH/эклава.tcl $@
fi

